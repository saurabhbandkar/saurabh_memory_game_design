if (localStorage.getItem("difficult")) {
    let difficultScore = document.getElementById("difficult-score");
    difficultScore.innerHTML = `Highscore: ${parseInt(localStorage.getItem("difficult"))}`;
} else {
    localStorage.setItem("difficult",0);
}

if (localStorage.getItem("easy")) {
    let easyScore = document.getElementById("easy-score");
    easyScore.innerHTML = `Highscore: ${parseInt(localStorage.getItem("easy"))}`;
} else {
    localStorage.setItem("easy",0);
}

if (localStorage.getItem("hard")) {
    let hardScore = document.getElementById("hard-score");
    hardScore.innerHTML = `Highscore: ${parseInt(localStorage.getItem("hard"))}`;
} else {
    localStorage.setItem("hard",0);
}