const gameContainer = document.getElementById("game");
let score = document.getElementById("score");
let currentScore = 0;
let successCards = 0;


const COLORS = [
  "black",
  "blue",
  "stark",
  "parkar",
  "banner",
  "thor",
  "scott",
  "green",
  "orange",
  "skyblue",
  "rogers",
  "black",
  "blue",
  "green",
  "orange",
  "skyblue",
  "tchala",
  "tchala",
  "rogers",
  "stark",
  "parkar",
  "banner",
  "thor",
  "scott"
];

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

let shuffledColors = shuffle(COLORS);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray) {
  for (let color of colorArray) {
    // create a new div
    const newDiv = document.createElement("div");

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(color);
    newDiv.classList.add("hide");

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}

let clickCounter = 0;
let openCards = [];
// TODO: Implement this function!
function handleCardClick(event) {
  // you can use event.target to see which element was clicked

  if (clickCounter == 2) {

  } else {
    event.target.classList.remove("hide");
    clickCounter += 1;
    console.log("you clicked", event.target);
    console.log(clickCounter);
    openCards.push(event.target);
    if (openCards.length == 2) {
      if (openCards[0].classList[0] != openCards[1].classList[0]) {
        setTimeout(() => {
          openCards[0].classList.add("hide");
          openCards[1].classList.add("hide");
          openCards = [];
          clickCounter = 0;
        }, 1000);
      } else {
        successCards += 2;
        openCards = [];
        clickCounter = 0;
      };
    };
  };
  currentScore += 1;
  score.innerHTML = `CURRENT SCORE: ${currentScore}`;
  if (successCards == 24) {
    alert("Congratulations, you won!!");
    if ((parseInt(localStorage.getItem("hard")) == 0) || (currentScore <= parseInt(localStorage.getItem("hard")))) {
      localStorage.setItem("hard", `${currentScore}`);
    };
  };
};
// when the DOM loads
createDivsForColors(shuffledColors);
